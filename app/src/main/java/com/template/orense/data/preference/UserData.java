package com.template.orense.data.preference;

import android.content.SharedPreferences;

import com.template.orense.data.model.api.UserItem;
import com.google.gson.Gson;

/**
 * Created by orense on 9/18/2017.
 */

public class UserData extends Data{

    public static final String USER_ITEM = "user_item";
    public static final String AUTHORIZATION = "authorization";


    public static void insert(UserItem userModel){
        SharedPreferences.Editor editor = Data.getSharedPreferences().edit();
        editor.putString(USER_ITEM, new Gson().toJson(userModel));
        editor.commit();
    }

    public static UserItem getUserModel(){
        UserItem userModel = new Gson().fromJson(Data.getSharedPreferences().getString(USER_ITEM, ""), UserItem.class);
        if(userModel == null){
            userModel = new UserItem();
        }
        return userModel;
    }

    public static boolean isLogin(){
        return getUserModel().id != 0;
    }

    public static boolean isMe(int id){
        return getUserModel().id == id;
    }

    public static int getUserId(){
        return getUserModel().id;
    }
}
