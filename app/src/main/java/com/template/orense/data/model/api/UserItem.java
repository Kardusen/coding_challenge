package com.template.orense.data.model.api;


import com.template.orense.vendor.android.base.AndroidModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by orense on 12/14/2016.
 */

public class UserItem extends AndroidModel {


    @SerializedName("username")
    public String username;

    @SerializedName("raffle_id")
    public String raffle_id;

    @SerializedName("email")
    public String email;

    @SerializedName("type")
    public String type;

    @SerializedName("fb_id")
    public String fbId;

    @SerializedName("is_verify")
    public boolean isVerify;

    @SerializedName("date")
    public Date date;

    @SerializedName("info")
    public Info info;

    @SerializedName("avatar")
    public Avatar avatar;

    public static class Date {
        @SerializedName("data")
        public Data data;

        public static class Data {
            @SerializedName("member_since")
            public MemberSince memberSince;

            public static class MemberSince {
                @SerializedName("date_db")
                public String dateDb;

                @SerializedName("month_year")
                public String monthYear;

                @SerializedName("time_passed")
                public String timePassed;

                @SerializedName("timestamp")
                public Timestamp timestamp;

                public static class Timestamp {
                    @SerializedName("date")
                    public String date;

                    @SerializedName("timezone_type")
                    public int timezoneType;

                    @SerializedName("timezone")
                    public String timezone;
                }
            }
        }
    }

    public static class Info {
        @SerializedName("data")
        public DataX data;

        public static class DataX {
            @SerializedName("fname")
            public String fname;
            @SerializedName("lname")
            public String lname;
            @SerializedName("name")
            public String name;
            @SerializedName("contact_number")
            public String contactNumber;
            @SerializedName("gender")
            public Object gender;
            @SerializedName("age")
            public Object age;
        }
    }

    public static class Avatar {
        @SerializedName("data")
        public DataXX data;

        public static class DataXX {
            @SerializedName("path")
            public Object path;
            @SerializedName("filename")
            public Object filename;
            @SerializedName("directory")
            public Object directory;
            @SerializedName("full_path")
            public String fullPath;
            @SerializedName("thumb_path")
            public String thumbPath;
        }
    }
}
