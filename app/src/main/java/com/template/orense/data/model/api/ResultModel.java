package com.template.orense.data.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



public class ResultModel {

    @SerializedName("artistName")
    @Expose
    private String artistName;

    @SerializedName("trackName")
    @Expose
    private String trackName;

    @SerializedName("previewUrl")
    @Expose
    private String previewUrl;

    @SerializedName("artworkUrl100")
    @Expose
    private String artworkUrl100;

    @SerializedName("primaryGenreName")
    @Expose
    public String primaryGenreName;

    @SerializedName("trackPrice")
    @Expose
    public String trackPrice;

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getPrimaryGenreName(){
        return primaryGenreName;
    }

    public void setPrimaryGenreName(String primaryGenreName){
        this.primaryGenreName = primaryGenreName;
    }

    public String getTrackPrice(){
        return trackPrice;
    }
    public void setPrice(String trackPrice){
        this.trackPrice = trackPrice;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    public String getArtworkUrl100() {
        return artworkUrl100;
    }

    public void setArtworkUrl100(String artworkUrl100) {
        this.artworkUrl100 = artworkUrl100;
    }
}