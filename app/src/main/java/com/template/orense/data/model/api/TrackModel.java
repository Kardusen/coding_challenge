package com.template.orense.data.model.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.template.orense.vendor.android.base.AndroidModel;

import java.util.List;

public class TrackModel extends AndroidModel {


    @SerializedName("resultCount")
    public int resultCount;
//    @SerializedName("results")
//    public List<ResultsBean> results;

        @SerializedName("wrapperType")
        public String wrapperType;
        @SerializedName("kind")
        public String kind;
        @SerializedName("artistId")
        public int artistId;
        @SerializedName("collectionId")
        public int collectionId;
        @SerializedName("trackId")
        public int trackId;
        @SerializedName("artistName")
        public String artistName;
        @SerializedName("collectionName")
        public String collectionName;
        @SerializedName("trackName")
        public String trackName;
        @SerializedName("collectionCensoredName")
        public String collectionCensoredName;
        @SerializedName("trackCensoredName")
        public String trackCensoredName;
        @SerializedName("artistViewUrl")
        public String artistViewUrl;
        @SerializedName("collectionViewUrl")
        public String collectionViewUrl;
        @SerializedName("trackViewUrl")
        public String trackViewUrl;
        @SerializedName("previewUrl")
        public String previewUrl;
        @SerializedName("artworkUrl30")
        public String artworkUrl30;
        @SerializedName("artworkUrl60")
        public String artworkUrl60;
        @SerializedName("artworkUrl100")
        public String artworkUrl100;
        @SerializedName("collectionPrice")
        public double collectionPrice;
        @SerializedName("trackPrice")
        public double trackPrice;
        @SerializedName("releaseDate")
        public String releaseDate;
        @SerializedName("collectionExplicitness")
        public String collectionExplicitness;
        @SerializedName("trackExplicitness")
        public String trackExplicitness;
        @SerializedName("discCount")
        public int discCount;
        @SerializedName("discNumber")
        public int discNumber;
        @SerializedName("trackCount")
        public int trackCount;
        @SerializedName("trackNumber")
        public int trackNumber;
        @SerializedName("trackTimeMillis")
        public int trackTimeMillis;
        @SerializedName("country")
        public String country;
        @SerializedName("currency")
        public String currency;
        @SerializedName("primaryGenreName")
        public String primaryGenreName;
        @SerializedName("isStreamable")
        public boolean isStreamable;
        @SerializedName("collectionArtistId")
        public int collectionArtistId;
        @SerializedName("collectionArtistViewUrl")
        public String collectionArtistViewUrl;
        @SerializedName("trackRentalPrice")
        public double trackRentalPrice;
        @SerializedName("collectionHdPrice")
        public double collectionHdPrice;
        @SerializedName("trackHdPrice")
        public double trackHdPrice;
        @SerializedName("trackHdRentalPrice")
        public double trackHdRentalPrice;
        @SerializedName("contentAdvisoryRating")
        public String contentAdvisoryRating;
        @SerializedName("shortDescription")
        public String shortDescription;
        @SerializedName("longDescription")
        public String longDescription;
        @SerializedName("hasITunesExtras")
        public boolean hasITunesExtras;
        @SerializedName("copyright")
        public String copyright;
        @SerializedName("description")
        public String description;
        @SerializedName("feedUrl")
        public String feedUrl;
        @SerializedName("artworkUrl600")
        public String artworkUrl600;
        @SerializedName("genreIds")
        public List<String> genreIds;
        @SerializedName("genres")
        public List<String> genres;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public TrackModel convertFromJson(String json) {
        return convertFromJson(json, TrackModel.class);
    }

    @SerializedName("results")
    @Expose
    public List<TrackModel> trackModels = null;

    public List<TrackModel> getResultModels() {
        return trackModels;
    }
}
