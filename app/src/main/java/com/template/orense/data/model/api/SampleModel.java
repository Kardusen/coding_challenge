package com.template.orense.data.model.api;

import com.template.orense.vendor.android.base.AndroidModel;
import com.google.gson.annotations.SerializedName;

/**
 * Created by orense on 8/12/2017.
 */

public class SampleModel extends AndroidModel {

    @SerializedName("name")
    public String name;

    @Override
    public String toString() {
        return convertToString(this);
    }

    @Override
    public SampleModel convertFromJson(String json) {
        return convertFromJson(json, SampleModel.class);
    }
}
