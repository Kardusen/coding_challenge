package com.template.orense.server.request;

import android.content.Context;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.template.orense.config.Keys;
import com.template.orense.config.Url;
import com.template.orense.data.model.api.TrackModel;
import com.template.orense.vendor.server.request.APIRequest;
import com.template.orense.vendor.server.request.APIResponse;
import com.template.orense.vendor.server.request.BaseRequest;
import com.template.orense.vendor.server.transformer.CollectionTransformer;
import com.template.orense.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public class Itunes {

//    public Itunes (){
//        super();
//    }

    public static Itunes getDefault(){
        return new Itunes();
    }

    public APIRequest track(Context context, String term){
        APIRequest apiRequest = new APIRequest<CollectionTransformer<TrackModel>>(context){
            @Override
            public Call<CollectionTransformer<TrackModel>> onCreateCall(){
                return getRetrofit().create(RequestService.class).requestAPI(Url.getSearch(), getMultipartBody());
            }

            @Override
            public void onResponse(){
                EventBus.getDefault().post((new TrackResponse(this)));
            }
        };

        apiRequest
                .addParameter(Keys.TERM, term)
//                .addParameter(Keys.ENTITY, track)
                .execute();
        return apiRequest;
    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<CollectionTransformer<TrackModel>> requestAPI(@Path("p") String p, @Part List<MultipartBody.Part> parts);

    }

    public class TrackResponse extends APIResponse<CollectionTransformer<TrackModel>>{
        public TrackResponse(APIRequest apiRequest){
            super(apiRequest);
        }
    }
}
