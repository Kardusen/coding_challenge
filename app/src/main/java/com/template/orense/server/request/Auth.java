package com.template.orense.server.request;

import android.content.Context;

import com.template.orense.config.Keys;
import com.template.orense.config.Url;
import com.template.orense.data.model.api.UserItem;
import com.template.orense.data.preference.UserData;
import com.template.orense.vendor.server.request.APIRequest;
import com.template.orense.vendor.server.request.APIResponse;
import com.template.orense.vendor.server.request.BaseRequest;
import com.template.orense.vendor.server.transformer.SingleTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by orense on 8/3/2017.
 */

public class Auth extends BaseRequest {

    public Auth(Context context) {
        super(context);
    }

    private String include = "date,info,avatar";

    public static Auth getDefault(Context context){
        return new Auth(context);
    }

    public void login(Context context, String username, String password) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestAPI(Url.getLogin(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LoginResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.INCLUDE, include)
                .addParameter(Keys.USERNAME, username)
                .addParameter(Keys.PASSWORD, password)
                .showDefaultProgressDialog("Signing in...")
                .execute();

    }

    public void logout(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestLogout(Url.getLogout(), getAuthorization(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LoginResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .showDefaultProgressDialog("Signing out...")
                .execute();
    }

    public APIRequest signUp(Context context,String fname, String lname, String contact, String email, String pass, String confirm_pass) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestAPI(Url.getSignUp(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new SignUpResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.FNAME, fname)
                .addParameter(Keys.LNAME, lname)
                .addParameter(Keys.EMAIL, email)
                .addParameter(Keys.CONTACT_NUMBER, contact)
                .addParameter(Keys.PASSWORD, pass)
                .addParameter(Keys.PASSWORD_CONFIRMATION, confirm_pass)
                .addParameter(Keys.INCLUDE, "info,avatar")
                .showDefaultProgressDialog("Signing up...")
                .execute();

        return apiRequest;

    }

    public APIRequest forgotPassword(Context context,String email) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestAPI(Url.getForgotPassword(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LoginResponse(this));
            }
        };
        apiRequest
                .addParameter(Keys.EMAIL,email)
                .showDefaultProgressDialog("Requesting code...")
                .execute();
        return apiRequest;

    }

    public APIRequest resetPassword(Context context,String email, String password, String confirmPassword, String token) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestAPI(Url.getResetPassword(), getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new ResetPasswordResponse(this));
            }
        };
        apiRequest
                .addParameter(Keys.EMAIL,email)
                .addParameter(Keys.PASSWORD,password)
                .addParameter(Keys.PASSWORD_CONFIRMATION,confirmPassword)
                .addParameter(Keys.VALIDATION_TOKEN,token)
                .showDefaultProgressDialog("Resetting password...")
                .execute();
        return apiRequest;

    }

    public void refreshToken(Context context) {
        APIRequest apiRequest = new APIRequest<SingleTransformer<UserItem>>(context) {
            @Override
            public Call<SingleTransformer<UserItem>> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestRefreshToken(Url.getRefreshToken(),getAuthorization(), getParameter());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new RefreshTokenResponse(this));
            }
        };

        apiRequest
                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
                .addParameter(Keys.INCLUDE, "info,avatar")
                .execute();

    }

    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserItem>> requestAPI(@Path("p") String p, @Part List<MultipartBody.Part> parts);

        @Multipart
        @POST("{p}")
        Call<SingleTransformer<UserItem>> requestLogout(@Path("p") String p, @Header("Authorization") String authorization, @Part List<MultipartBody.Part> parts);

        @POST("{p}")
        Call<SingleTransformer<UserItem>> requestRefreshToken(@Path("p") String p, @Header("Authorization") String authorization, @Body HashMap<String, String> map);

    }

    public class LoginResponse extends APIResponse<SingleTransformer<UserItem>> {
        public LoginResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class RefreshTokenResponse extends APIResponse<SingleTransformer<UserItem>> {
        public RefreshTokenResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class ResetPasswordResponse extends APIResponse<SingleTransformer<UserItem>> {
        public ResetPasswordResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }

    public class SignUpResponse extends APIResponse<SingleTransformer<UserItem>> {
        public SignUpResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
