package com.template.orense.vendor.server.util;

import com.template.orense.data.model.api.ResultModel;

public interface ResultClickListener {
    void onResultItemClick(ResultModel resultModel);
}
