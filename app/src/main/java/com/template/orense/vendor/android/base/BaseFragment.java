package com.template.orense.vendor.android.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.template.orense.R;
import com.template.orense.vendor.android.java.Log;

import java.lang.reflect.Method;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import icepick.Icepick;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by orense on 3/16/2017.
 */

public class BaseFragment extends Fragment {

    private Unbinder unbinder;
    private Context context;
    private View views;
    private Class baseActivity;
    private CompositeDisposable mDisposables = new CompositeDisposable();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(onLayoutSet(), container, false);
        views = view;
        bindView(view);
        context = getActivity();
        onViewReady();
        onViewReady(savedInstanceState);
        return view;
    }

    public Context getContext(){
        return context;
    }

    public View getView(){
        return views;
    }

    public int onLayoutSet(){
        return R.layout.fragment_default;
    }

    public void setParentActivity(Class baseActivity){
        this.baseActivity = baseActivity;
    }

    public Method[] getParentActivity(){
        return baseActivity.getMethods();
    }

    public int onLayoutSet(@LayoutRes int layout){
        return layout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Icepick.saveInstanceState(this, outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        unbindView();
        super.onDestroyView();
    }

    public void onViewReady(){

    }

    public void onViewReady(Bundle savedInstanceState){

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
//        Glide.with(this).onLowMemory();
        Glide.get(getContext()).clearDiskCache();
        Glide.get(getContext()).clearMemory();
    }

    private void bindView(View view){
        unbinder = ButterKnife.bind(this, view);
    }

    private void unbindView(){
        unbinder.unbind();
    }


    public void subscribe(Disposable disposable) {
        mDisposables.add(disposable);
    }

    public void unsubscribe() {
        if (mDisposables != null && !mDisposables.isDisposed()) {
            mDisposables.dispose();
            mDisposables.clear();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if(fragment != null){
                    fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if(fragment != null){
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }

}
