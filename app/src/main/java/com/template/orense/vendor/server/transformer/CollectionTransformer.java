package com.template.orense.vendor.server.transformer;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by orense on 8/3/2017.
 */

public class CollectionTransformer<T> extends BaseTransformer{

    @SerializedName("data")
    public List<T> data;
}
