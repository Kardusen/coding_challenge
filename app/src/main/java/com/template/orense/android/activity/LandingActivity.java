package com.template.orense.android.activity;

import com.template.orense.R;
import com.template.orense.android.fragment.landing.SplashFragment;
import com.template.orense.android.route.RouteActivity;

/**
 * Created by orense on 8/12/2017.
 */

public class LandingActivity extends RouteActivity {
    public static final String TAG = LandingActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_landing;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "splash":
                openSplashFragment();
                break;
            default:
                openSplashFragment();
                break;
        }
    }

    public void openSplashFragment(){ switchFragment(SplashFragment.newInstance()); }
}
