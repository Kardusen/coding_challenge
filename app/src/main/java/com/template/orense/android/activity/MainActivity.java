package com.template.orense.android.activity;

import com.template.orense.R;
import com.template.orense.android.fragment.home.HomeFragment;
import com.template.orense.android.route.RouteActivity;

/**
 * Created by orense on 8/12/2017.
 */

public class MainActivity extends RouteActivity {
    public static final String TAG = MainActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_main;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "main":
                openHomeFragment();
                break;
                default:
                    openHomeFragment();
                break;
        }
    }

    public void openHomeFragment(){
        switchFragment(HomeFragment.newInstance());
    }
}
