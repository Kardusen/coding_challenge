package com.template.orense.android.fragment.home;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.template.orense.R;
import com.template.orense.android.activity.MainActivity;
import com.template.orense.android.adapter.SearchResultRVAdapter;
import com.template.orense.android.adapter.TrackAdapter;
import com.template.orense.data.model.api.ResultModel;
import com.template.orense.data.model.api.SearchResultModel;
import com.template.orense.server.request.SearchService;
import com.template.orense.vendor.android.base.BaseFragment;
import com.template.orense.vendor.android.java.EndlessRecyclerViewScrollListener;
import com.template.orense.vendor.server.util.RetrofitBuilder;


import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends BaseFragment implements
        EndlessRecyclerViewScrollListener.Callback, SearchResultRVAdapter.ResultClickListener, Callback<SearchResultModel> {
    public static final String TAG = HomeFragment.class.getName();

    private MainActivity mainActivity;
    private TrackAdapter trackAdapter;
    private LinearLayoutManager linearLayoutManager;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private PublishSubject<CharSequence> queryObservable;
    private static final int SEARCH_TIMEOUT_MILLI = 555;
    private SearchService searchService;
    private SearchResultRVAdapter searchResultRVAdapter;

    @BindView(R.id.trackRV)             RecyclerView trackRV;
    @BindView(R.id.searchET)            EditText searchET;
    @BindView(R.id.progressPB)          ProgressBar progressPB;


    public static HomeFragment newInstance(){
        HomeFragment homeFragment = new HomeFragment();
        return homeFragment;
    }

    @Override
    public int onLayoutSet(){
      return  R.layout.fragment_home;
    }

    @Override
    public void onViewReady(){
        mainActivity = (MainActivity) getContext();
        attemptSearch();
    }
    private void attemptSearch(){

        searchService = RetrofitBuilder.getRetrofit().create(SearchService.class);

        queryObservable = PublishSubject.create();
        queryObservable.debounce(SEARCH_TIMEOUT_MILLI, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CharSequence>() {
                    @Override public void onSubscribe(Disposable d) { }
                    @Override public void onComplete() { }

                    @Override
                    public void onNext(CharSequence s) {
                        if (s.length() > 0) {
                            progressPB.setVisibility(View.VISIBLE);
//                            player.stop();
                            searchService.getSearchResults(s, SearchService.ENTITY_TYPE_MUSIC_TRACK)
                                    .enqueue(HomeFragment.this);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
//                        player.stop();
                    }
                });

        searchET.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void afterTextChanged(Editable s) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                queryObservable.onNext(s);
            }
        });

//        // Search result recycler view
        linearLayoutManager = new LinearLayoutManager(getContext());
        trackAdapter = new TrackAdapter(getContext());
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager, this);
        trackRV.addOnScrollListener(endlessRecyclerViewScrollListener);
        searchResultRVAdapter = new SearchResultRVAdapter(this);
        trackRV.setLayoutManager(linearLayoutManager);
        trackRV.setAdapter(searchResultRVAdapter);
    }



    @Override
    public void onResponse(Call<SearchResultModel> call, Response<SearchResultModel> response) {
        progressPB.setVisibility(View.GONE);

        if (response.isSuccessful() && response.body() != null) {
            List<ResultModel> resultModelList = response.body().getResultModels();
            searchResultRVAdapter.updateResults(resultModelList);

            if (resultModelList.size() > 0) {
                trackRV.setVisibility(View.VISIBLE);
            } else {
                trackRV.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onFailure(Call<SearchResultModel> call, Throwable t) {
        progressPB.setVisibility(View.GONE);
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onResultItemClick(ResultModel resultModel) {

    }
}
