package com.template.orense.android.adapter;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.template.orense.R;
import com.template.orense.data.model.api.TrackModel;
import com.template.orense.vendor.android.base.BaseRecylerViewAdapter;

import java.util.List;

import butterknife.BindView;

public class TrackAdapter extends BaseRecylerViewAdapter<TrackAdapter.ViewHolder, TrackModel> {

    private ClickListener clickListener;
    public List<TrackModel> trackModels;

    public TrackAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_track));
    }

    @Override
    public int getItemCount() {
        return trackModels != null ? trackModels.size() : 0;
    }

    public void updateResults(List<TrackModel> trackModels) {
        this.trackModels = trackModels;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.adapterCON.setTag(holder.getItem());
        holder.trackTXT.setText(holder.getItem().trackName);
        holder.genreTXT.setText(holder.getItem().primaryGenreName);
        holder.priceTXT.setText(holder.getItem().trackPrice + "");


        Glide.with(getContext())
                .load(holder.getItem().artworkUrl100)
                .centerCrop()
                .into(holder.artworkIV);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.adapterCON)          View adapterCON;
        @BindView(R.id.artworkIV)              ImageView artworkIV;
        @BindView(R.id.trackTXT)             TextView trackTXT;
        @BindView(R.id.genreTXT)             TextView genreTXT;
        @BindView(R.id.priceTXT)             TextView priceTXT;

        public ViewHolder(View view) {
            super(view);
            adapterCON.setOnClickListener(TrackAdapter.this);
        }

        public TrackModel getItem() {
            return (TrackModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:
                if (clickListener != null){
                    clickListener.onItemClick((TrackModel) v.getTag());
                }

                break;

        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(TrackModel trackModel);
    }
}
