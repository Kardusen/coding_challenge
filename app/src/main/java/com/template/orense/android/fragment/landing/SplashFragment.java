package com.template.orense.android.fragment.landing;

import android.os.Handler;

import com.template.orense.R;
import com.template.orense.android.activity.LandingActivity;
import com.template.orense.vendor.android.base.BaseFragment;


public class SplashFragment extends BaseFragment {

    public static final String TAG = SplashFragment.class.getName().toString();

    private LandingActivity landingActivity;
    private Handler handler;
    private Runnable runnable;

    public static SplashFragment newInstance() {
        SplashFragment fragment = new SplashFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_splash;
    }

    @Override
    public void onViewReady() {
        landingActivity = (LandingActivity) getContext();
        attempt();
    }


    private void attempt(){

        runnable = () -> landingActivity.runOnUiThread(() -> landingActivity.startMainActivity("main"));

        handler = new Handler();
        handler.postDelayed(runnable, 3000);

    }

    @Override
    public void onResume() {
        super.onResume();
    }




}
