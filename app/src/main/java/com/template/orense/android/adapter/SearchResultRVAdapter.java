package com.template.orense.android.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.template.orense.R;
import com.template.orense.data.model.api.ResultModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchResultRVAdapter extends RecyclerView.Adapter<SearchResultRVAdapter.ViewHolder> {

    private List<ResultModel> resultModelList;
    private ResultClickListener resultClickListener;

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.artworkIV)                       ImageView artworkIV;
        @BindView(R.id.trackTXT)                        TextView trackTXT;
        @BindView(R.id.authorTXT)                       TextView authorTXT;
        @BindView(R.id.genreTXT)                        TextView genreTXT;
        @BindView(R.id.priceTXT)                        TextView priceTXT;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        void bindData(ResultModel resultModel) {
            trackTXT.setText(resultModel.getTrackName());
            priceTXT.setText( "$" + resultModel.getTrackPrice());
            authorTXT.setText(resultModel.getArtistName());
            genreTXT.setText(resultModel.getPrimaryGenreName());
            Glide.with(artworkIV.getContext())
                    .load(resultModel.getArtworkUrl100())
                    .diskCacheStrategy(DiskCacheStrategy.DATA)
                    .placeholder(R.drawable.music_player)
                    .into(artworkIV);
        }

        @Override
        public void onClick(View v) {
            int pos = getAdapterPosition();
            if (pos != RecyclerView.NO_POSITION && resultClickListener != null) {
                resultClickListener.onResultItemClick(resultModelList.get(pos));
            }
        }
    }

    public SearchResultRVAdapter(ResultClickListener resultClickListener) {
        this.resultClickListener = resultClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_track, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(resultModelList.get(position));
    }

    @Override
    public int getItemCount() {
        return resultModelList != null ? resultModelList.size() : 0;
    }

    public void updateResults(List<ResultModel> resultModelList) {
        this.resultModelList = resultModelList;
        notifyDataSetChanged();
    }

    public interface ResultClickListener {
        void onResultItemClick(ResultModel resultModel);
    }
}
