package com.template.orense.config;

import com.template.orense.vendor.android.java.security.SecurityLayer;

/**
 * Created by orense on 8/3/2017.
 */

public class Url extends SecurityLayer {
    public static final String PRODUCTION_URL = decrypt("https://itunes.apple.com/");
    public static final String DEBUG_URL = decrypt("https://itunes.apple.com/");

    public static final String APP = App.production ? PRODUCTION_URL : DEBUG_URL;

    public static final String getSignUp(){return "/api/auth/register.json";}

    public static final String getLogin(){return "/api/auth/login.json";}

    public static final String getLogout(){return "/api/auth/logout.json";}

    public static final String getForgotPassword(){return "/api/auth/forgot-password.json";}

    public static final String getResetPassword(){return "/api/auth/reset-password.json";}

    public static final String getRefreshToken(){return "/api/auth/refresh-token.json";}

    public static final String getSearch(){return "search";}

}
