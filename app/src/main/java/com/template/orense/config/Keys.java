package com.template.orense.config;

/**
 * Created by orense on 8/7/2017.
 */

public class Keys {
    public static String DEVICE_ID = "device_id";
    public static String DEVICE_NAME = "device_name";
    public static String DEVICE_REG_ID = "device_reg_id";
    public static String API_TOKEN = "api_token";
    public static String PAGE = "page";
    public static String PER_PAGE = "per_page";
    public static String USER_ID = "user_id";
    public static String USERNAME = "username";
    public static String PASSWORD = "password";
    public static String INCLUDE = "include";
    public static String AUTH_ID = "auth_id";
    public static String EMAIL = "email";
    public static String FNAME = "fname";
    public static String LNAME = "lname";
    public static String CONTACT_NUMBER = "contact_number";
    public static String PASSWORD_CONFIRMATION = "password_confirmation";
    public static String VALIDATION_TOKEN = "validation_token";
    public static String TERM = "term";
    public static String COUNTRY = "country";
    public static String MEDIA = "media";
    public static String ENTITY = "entity";
    public static String ATTRIBUTE = "attribute";
    public static String ENTITY_TYPE_MUSIC_TRACK = "musicTrack";

}
